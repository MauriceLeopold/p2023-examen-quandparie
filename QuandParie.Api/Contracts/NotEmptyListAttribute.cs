﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace QuandParie.Api.Contracts
{
    public class NotEmptyListAttribute: ValidationAttribute
    {
        public override bool IsValid(object value) 
            => value is IList { Count: > 0 };
    }
}
