﻿using System.ComponentModel.DataAnnotations;

namespace QuandParie.Api.Contracts
{
    public record CustomerCreation(
        [Required] string Email,
        [Required] string FirstName,
        [Required] string LastName);
}
