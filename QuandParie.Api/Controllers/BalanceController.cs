﻿using Microsoft.AspNetCore.Mvc;
using QuandParie.Api.Contracts;
using QuandParie.Core.Application;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace QuandParie.Api.Controllers
{
    [ApiController]
    public class BalanceController : ControllerBase
    {
        private readonly BalanceApplication application;

        public BalanceController(BalanceApplication application)
        {
            this.application = application;
        }

        [HttpPost("/customers/{email}/receivePayment")]
        public async Task<ActionResult> ReceivePayment(string email)
        {
            try
            {
                await application.ReceivePayment(email, await ReadBodyAsStringAsync());
                return Ok();
            }
            catch (ArgumentException argumentException)
            {
                return BadRequest(argumentException.Message);
            }
        }

        private Task<string> ReadBodyAsStringAsync()
            => new StreamReader(Request.Body).ReadToEndAsync();

        [HttpPost("/customers/{email}/emitTransfer")]
        public async Task<ActionResult> EmitTransfer(string email, TransferCreation transfer)
        {
            try
            {
                await application.EmitTransfer(email, transfer.Iban, transfer.Amount);
                return Ok();
            }
            catch (ArgumentException argumentException)
            {
                return BadRequest(argumentException.Message);
            }
        }
    }
}
