﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Gateway.Contracts
{
    public record ReceivedPaymentData(string Token, int Amount);
}
